const express = require('express')

const app = express()

const port = 80
const defaultResponse = "Ok, Node"

app.get('/', (req, res) => {
  res.send(defaultResponse)
})

app.get('/node/ric', (req, res) => {
  res.send(defaultResponse)
})

app.get('/proxy/', (req, res) => {
  setTimeout(() => {
    res.send(defaultResponse)
  }, 3000)
})

app.get('/intensive/', (req, res) => {
  let start = new Date();
  while ((start.getSeconds() - (new Date()).getSeconds()) < 3) { }
  res.send(defaultResponse)
})

var guarani = require('./guarani');
app.use('/guarani', guarani);

app.listen(port)
