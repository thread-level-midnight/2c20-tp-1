var express = require('express');
var router = express.Router();

/* Visita a la pagina de login. Solo devuelve contenido estatico. */
const TIEMPO_GET_LOGIN = 0;
const PROC_GET_LOGIN = 500;

/* Post del formulario de login. Consulta a la base de datos y
  escritura a los datos de sesion. */
const TIEMPO_LOGIN = 500;
const PROC_LOGIN = 1000000;

/* Eleccion de carrera. Consulta a la base de datos. */
const TIEMPO_CARRERA = 1000;
const PROC_CARRERA = 1000;

/* Consulta a mis inscripciones. Consulta a la base de datos. */
const TIEMPO_INSCRIPCIONES = 1000;
const PROC_INSCRIPCIONES = 1000;

/* Consulta de las materias disponibles. Consulta a la base de datos. */
const TIEMPO_DISPONIBLES = 1000;
const PROC_DISPONIBLES = 1000;

/* Inscripcion a materia. Validacion y escritura a la base de datos. */
const TIEMPO_INSCRIPCION = 3000;
const PROC_INSCRIPCION = 10000000;

/* Logout. Escritura a los datos de sesion. */
const TIEMPO_LOGOUT = 0;
const PROC_LOGOUT = 1000000;

router.get('/login/', function(req, res) {
  for (let _ = 0; _ < PROC_GET_LOGIN; _++) {}
  setTimeout(() => {
    res.send('<h1>Login page</h1>');
  }, TIEMPO_GET_LOGIN)
});

router.post('/login/', function(req, res) {
  for (let _ = 0; _ < PROC_LOGIN; _++) {}
  setTimeout(() => {
      res.send("<h1>Login exitoso</h1>");
  }, TIEMPO_LOGIN);
});

router.get('/carreras/:carrera/', function(req, res) {
  for (let _ = 0; _ < PROC_CARRERA; _++) {}
  setTimeout(() => {
    res.send(`<h1>Carrera ${req.params.carrera}</h1>`);
  }, TIEMPO_CARRERA);
});

router.get('/mis-inscripciones/', function(req, res) {
  for (let _ = 0; _ < PROC_INSCRIPCIONES; _++) {}
  setTimeout(() => {
    res.send("<h1>Tus inscripciones</h1>");
  }, TIEMPO_INSCRIPCIONES);
});

router.get('/materias-disponibles/', function(req, res) {
  for (let _ = 0; _ < PROC_DISPONIBLES; _++) {}
  setTimeout(() => {
    res.send("<h1>Materias disponibles</h1>");
  }, TIEMPO_DISPONIBLES);
});

router.post('/inscribirme/', function(req, res) {
  for (let _ = 0; _ < PROC_INSCRIPCION; _++) {}
  setTimeout(() => {
    res.send("<h1>Inscripcion exitosa</h1>");
  }, TIEMPO_INSCRIPCION);
});

router.post('/logout/', function(req, res) {
  for (let _ = 0; _ < PROC_LOGOUT; _++) {}
  setTimeout(() => {
    res.send("<h1>Logout exitoso</h1>");
  }, TIEMPO_LOGOUT);
});

module.exports = router;
