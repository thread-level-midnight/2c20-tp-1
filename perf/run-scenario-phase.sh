#!/bin/sh
npm run artillery -- run -e $1 -c phases/$2.yaml scenarios/$3.yaml | grep -E 'Started|high|finished|Summary'
