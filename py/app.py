from time import sleep

from flask import Flask
app = Flask(__name__)

default_response = "Ok, Gunicorn"

@app.route('/')
def ping():
  return default_response

@app.route('/proxy/')
def proxy():
  sleep(3)
  return default_response


from datetime import datetime

@app.route('/intensive/')
def intensive():
  start = datetime.now()
  while (datetime.now() - start).seconds < 3:
    pass
  return default_response
