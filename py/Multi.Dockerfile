FROM python:3.8.6

WORKDIR /app

COPY . .

RUN pip install -r requirements.txt

EXPOSE 80

ENV FLASK_APP=app.py
# USER 1001
CMD gunicorn --log-file=- --reload -w 4 --bind 0.0.0.0:80 wsgi:app
