# Trabajo Práctico 1

## Objetivos

El objetivo principal es comparar algunas tecnologías, ver cómo diversos aspectos impactan en los atributos de calidad y probar, o al menos sugerir, qué cambios se podrían hacer para mejorarlos.
El objetivo menor es que aprendan a usar una variedad de tecnologías útiles y muy usadas hoy en día, incluyendo:

- Node.js (+ Express)
- Python (+ Flask y Gunicorn)
- Docker
- Docker-compose
- Nginx
- Algún generador de carga (la propuesta es usar Artillery, pero pueden cambiarlo)
- Alguna forma de tomar mediciones varias y visualizarlas, preferentemente en tiempo real, con persistencia, y en un dashboard unificado (la propuesta es usar el plugin de Artillery + cAdvisor + StatsD + Graphite + Grafana, pero pueden cambiarlo).

## Sección 1

### Node vs Gunicorn

El primer análisis consiste en someter a ambos servidores a distintos escenarios de carga para evaluar cómo responde cada uno en estos casos. Queremos ver qué servidor es más conveniente para distintos tipos de uso.

Para hacer esto vamos a comparar a ambos servidores con un solo container y un solo worker cada uno, usando los tres endpoint pedidos: ping, proxy e intensivo.

El escenario consiste en una prueba *scalability*. Así podremos analizar niveles de estrés a los servidores y vemos el momento en que cada uno empieza a fallar o demorar más.

Las fases son:

1. Una primer fase de warm up con 10 requests por segundo durante 30 segundos
2. Una rampa de 10 req. p/s a 100 req. p/s con duración de 120s.
3. La fase final será de 10 req. p/s durante 30s.

#### **RESULTADOS**

Se muestra para cada endpoint un gráfico de grafana para cada servidores

#### *Ping*

Empezaremos efectuando el test en el endpoint de ping de las instancias de Node:
![](./imagenes/scalability_ping_node_1.png)
![](./imagenes/scalability_ping_node_2.png)

Vemos que durante toda la ejecución, se respondieron todas las requests con código 200, respondiendo en promedio a un tiempo de 58ms por request, utilizando muy pocos recursos (CPU y memoria).

Vemos ahora cómo se comporta la instancia de Gunicorn:
![](./imagenes/scalability_ping_gunicorn_1.png)
![](./imagenes/scalability_ping_gunicorn_2.png)

Por su parte, vemos que Gunicorn se comportó de manera similar a su contraparte, respondiendo todos los requests con un *status code* 200, en un tiempo promedio mayor (59ms) pero utilizando menos recursos (aproximadamente un 0,058% de CPU contra 7.59% de Node)

Como conclusiones a este primer test sobre un endpoint de procesamiento mínimo, ambos tipos de instancias se comportan de manera similar, actuando de manera correcta en todos los casos.

#### *Proxy*

En esta oportunidad haremos uso del endpoint */proxy*, el cual simula que el servicio efectúa una llamada o consulta a otro servicio externo y espera por el resultado, en donde el procesamiento es mínimo pero implica una demora en la respuesta. Comenzando por Node:

![](./imagenes/scalability_proxy_node_1.png)
![](./imagenes/scalability_proxy_node_2.png)

Observando los gráficos, vemos que Node responde muy bien a esta situación, donde:
- Responde correctamente todas las peticiones con código 200,
- responde en un tiempo promedio de 3,087s (lo cuál es lógico debido a la "demora simulada" donde el servicio "duerme" por 3 segundos) y,
- utiliza casi los mismos recursos que en el caso de la sección anterior, utilizando un 7.40% del CPU en promedio.

Por otro lado, en Gunicorn:
![](imagenes/scalability_proxy_gunicorn_1.png)
![](imagenes/scalability_proxy_gunicorn_2.png)

Los resultados arrojados muestran que:
- Responde todos los request con status code 200,
- responde en un tiempo promedio un poco más alto que su contraparte (3,122ms)
- Hace uso de menor cantidad de recursos que node

En conclusión, ambos se comportan de manera similar, respondiendo todos los requests correctamente, con una leve mejora por parte de Gunicorn quien hace uso de menor cantidad de recursos.

#### *Intensivo*

En esta sección, hacemos uso del endpoint */intensive* el cual simula el uso intensivo de cálculos y procesamiento de datos por parte del servidor.
Para el caso de Node:
![](./imagenes/scalability_intensive_node_1.png)
![](./imagenes/scalability_intensive_node_2.png)

Para este caso, donde se simula el uso intensivo del servidor, vemos que Node ya empieza a mostrar algunas fallas. Como vemos, aproximadamente cuando se ejecutan 60 requests por segundo (simulando el uso del servidor por parte de 60 usuarios "virtuales"), el servidor comienza a rechazar requests con código 500, **manteniendo constante** la cantidad de request que **si** puede procesar y devolver 200. También, el tiempo promedio de respuesta del servidor aumenta cuantos más requests por segundo debe procesar, como se puede ver en la gráfica.

Por su parte, tenemos los resultados obtenidos de la ejecución de este mismo escenario en la instancia de Gunicorn:
![](imagenes/scalability_intensive_gunicorn_1.png)
![](imagenes/scalability_intensive_gunicorn_2.png)

El caso de Gunicorn parece no mejorar con respecto a su contraparte. En algunos casos hasta parecería que su funcionamiento empeoró: la cantidad de requests con resultado 200 es muy inferior a aquellas rechazadas con errores 500 o 502, código que hasta ahora no hemos visto. Por otro lado, el tiempo de respuesta promedio, además de incrementar más cuanto más grande es la carga, tiene un promedio de respuesta más grande: casi 4 veces más grande que el obtenido en Node.

#### **CONCLUSIONES**
Algunas conclusiones de los análisis anteriores:

- En las operaciones sencillas, gunicorn consume menos CPU que Node
- En operaciones de uso intenso de la CPU, Node parece responder de mejor manera, no solo en cuanto a tiempo y recursos consumidos, sino a la cantidad de respuestas con resultado "exitoso".

### Recuperación Node vs. Gunicorn
Siguiendo los resultados de la sección anterior, nos gustaría saber cual es la capacidad de cada servidor en recuperarse luego de sufrir una falla.

Para eso, sabiendo que ante cierta carga ambos comienzan a fallar, entonces intentaremos conseguir que fallen para luego bajar drásticamente la carga y analizar si ambos logran recuperarse a la misma velocidad o al mismo tiempo. Para esto, el escenario que planteamos es el siguiente:
- Etapa de Warm Up previa, de carga 10 rps durante 30s.
- Etapa de "break" de la aplicación, donde se la someterá a una tasa de 100 rps durante otros 30s, para forzar la caída del servicio.
- Etapa de análisis de recuperación, donde la carga bajará nuevamente a 10 rps durante 60s.

Los resultados para Node son:

![](imagenes/recoverability_node_1.png)

Como podemos ver, durante la etapa de quiebre de la aplicación logramos "romperla" ya que aparecen los códigos de error 500, pero llegada la etapa de baja de carga vemos que Node responde muy satisfactoriamente: apenas 1 solo request recibido con código de error 500, mientras que todo el resto recibe una respuesta correcta.

Por otro lado, Gunicorn:
![](imagenes/recoverability_gunicorn_1.png)

Como vemos, no podemos decir lo mismo de este tipo de servidor: pasada la etapa de quiebre, donde logramos romper la aplicación, nunca vuelve a recuperarse de la misma forma que Node. Los errores 500 y 502 persisten durante toda la etapa de carga baja, con menor intensidad que en la etapa de quiebre.

#### **CONCLUSIONES**

Node se recupera instantáneamente cuando baja la carga. Es decir, el error no perdura en el tiempo. En cambio, Gunicorn sigue fallando. En conclusión, podemos decir que el servidor programado en Node tiene mejor recuperabilidad que su contraparte en Gunicorn.

Si consideramos que la **disponibilidad** de un sistema se construye a partir de la confiabilidad del mismo, sumado a lo que es la recuperabilidad (es decir, la habilidad de repararse a sí mismo luego de una falla) que vemos que en este caso no favorece a Gunicorn, entonces podríamos concluir que este resultado también expone conclusiones sobre la disponibilidad de ambos sistemas: podríamos decir que es mejor la disponibilidad del sistema Node que el Gunicorn.

Otro punto donde podríamos notar este impacto sobre la disponibilidad, es si consideramos esta otra definición de dicho atributo de calidad que se propone en *Software Architecture In Practice* del autor Len Bass:
```
The availability of a system can be calculated as the probability that it will provide the specified services within required bounds over a specified time interval.
```
Considerándola como la *probabilidad de que un sistema se encuentre operativo cuando se lo precisa*, podríamos calcular dicha probabilidad de la siguiente forma:

$$  \alpha = \frac{mean \ time \ to \ failure}{mean \ time \ to \ failure + mean \ time \ to \ repair} $$

donde mean time to repair (o MTTR) sin dudas será menor en el sistema Node que en el sistema Gunicorn, haciendo que esta probabilidad sea mayor en el primero de ellos.

### Gunicorn Async vs Sync
En esta sección vamos a comparar gunicorn con workers asincrónicos contra sincrónicos. Como fases usamos un método de escalera en la que está compuesta por una fase plana de 20 segundos y otra rampa de 20 en la que subimos 10 rps en 20 segundos, repitiendo este proceso hasta 60 rps, continuando con una  meseta de 100 rps y terminando con la fase final para ver si se estabiliza, el archivo yaml de la fase es *step_to_n.yaml*

Estas medidas fueron tomadas con el endpoint de proxy.


**Análisis**
En la siguiente imagen se puede visualizar como el gunicorn sync rápidamente empieza a generar request de error.
![](imagenes/gunicorn_multi_step_to_n_proxy1.png)

Mientras que en el caso del gunicorn con workers async obtenemos los siguientes resultados

Gunicorn Async vs Sync


#### **CONCLUSIONES**
Vemos que en el momento de manejar carga el worker sincronico sufre a la hora de manejar la carga, mientras que el asincrónico logra manejar la carga sin problemas.
Con esta información procedemos a la siguiente sección para ver si  lleva a buscar compararlo con node en un uso intenso, que es lo que se visualiza en la siguiente sección

### Intense endpoint en instancias asincronicas
En esta sección vamos a comparar cómo se comportan node usando una instancia, node usando 4 instancias, y el multi worker async de gunicorn de la anterior sección

#### Node 1 instancia

Cuando hacemos uso de una instancia de node sola, podemos visualizar los siguientes gráficos

![](imagenes/node1_step_to_n_intense1.png)
![](imagenes/node1_step_to_n_intense2.png)

Vemos que es capaz de soportar alrededor de 30 rps, antes de empezar a tirar error, también vemos como el CPU llega al máximo por momentos.
#### Node 4 instancias

El dashboard es el siguiente al utilizar 4 instancias de node
![](imagenes/node4_step_to_n_intense1.png)
![](imagenes/node4_step_to_n_intense2.png)

Podemos ver como aguanta casi toda la carga con solamente 30 errores 500 y como el CPU máximo no llega a su máximo uso, visualizando
las demás instancias también se vio un valor alrededor de ese porcentaje sin mucha diferencia


#### Gunicorn asincrónico

En este gunicorn usamos 4 workers asincrónicos, con el siguiente dashboard como resultado

![](imagenes/gunicorn_async_step_to_n_intense1.png)
![](imagenes/gunicorn_async_step_to_n_intense2.png)

Vemos cómo rápidamente comenzó a mandar errores 500 por culpa de la carga en el endpoint intenso
pero también pudimos remarcar que fue capaz de recuperarse al terminar con el test. Además se puede ver como el máximo de CPU llega
a ser el total de la máquina utilizada (4 cores - 400%).

#### **CONCLUSIONES**

Node con una sola instancia, ya posee un mejor comportamiento a la hora de hacer muchas request intensas aunque empieza a sufrir por momentos
pero al agregarle 3 instancias vemos cómo aprovecha mejor los recursos, logrando soportar la carga con leves fallas.
Mientras que con gunicorn vemos que sufre casi inmediatamente y comienza a mandar mensajes 500 rápidamente lo cual no sería adecuado utilizarlo para este tipo
de endpoint ya que demostró que no podía aguantar la carga. Pudimos remarcar que por lo menos las 3 arquitecturas analizadas en esta sección
lograron recuperarse al tener errores, por lo que al usar estas tecnologias asíncronas vemos que logramos el atributo de calidad de *recoverability*


---

## Sección 2

En esta sección procederemos a realizar un análisis similar al de la sección anterior, pero en esta oportunidad utilizando un servicio web provisto por la cátedra, de la cual no tenemos información sobre su implementación.

Utilizando las estrategias aprendidas en la sección anterior, la idea será caracterizar cada uno de los servicios que este contenedor expone, para determinar:

1. Cuál de ellos se comporta de manera *Sincrónica* y cuál de forma *Asincrónica*
2. Cuál es el tiempo que tardan en responder requests, y
3. Para el caso del *sincrónico* determinar la cantidad de workers con la que fue implementado.


La estrategia será, entonces, someter a los servicios web a los mismos escenarios a los que fueron expuestos los servicios de la sección anterior, y comparar los resultados obtenidos con los que logramos anteriormente para poder categorizar cada servicio de esta "black-box".

### Rampa

Comenzaremos entonces con el primer escenario ejecutado, el cual consistia de las siguientes fases:

1. Una primer fase de warm up con 10 requests por segundo durante 30 segundos
2. Una rampa de 10 req. p/s a 100 req. p/s con duración de 120s.
3. La fase final será de 10 req. p/s durante 30s.

Comenzando por el endpoint `bbox-1`, mapeado en el puerto 9090 del contenedor.

![](./imagenes/seccion_2_bbox_1_ramp.png)
![](./imagenes/seccion_2_bbox_1_ramp_2.png)

Siguiendo por el endpoint `bbox-2`, mapeado en el puerto 9091 del contenedor:

![](imagenes/seccion_2_bbox_2_ramp_1.png)
![](imagenes/seccion_2_bbox_2_ramp_2.png)

Comparando las gráficas de ambos casos entre sí y con las análogas de la sección 1 podemos ver que:

- `bbox-1` no respondió bien ante la exigencia. Muy temprano en la ejecución comenzaron a aparecer los primeros errores 500 o 504, muy similar a lo ocurrido con el servicio Gunicorn de la primera parte. Por otro lado, en cuanto a respuestas, `bbox-2` no devolvió ningún error, soportando toda la carga que impone la rampa durante todo el escenario, similar a lo que se vio con el servicio Node de la primera sección.
- En cuanto al tiempo de respuesta de requests, los correspondientes al `bbox-2` se mantuvieron constantes durante toda la ejecución, en promedio unos 1,3 segundos. Para el caso de `bbox-1`, los mismos variaron en todo el transcurso de la ejecución. Esto puede deberse a la cantidad de requests rechazados por "timeout" (504) y por la exigencia sometida al servicio.
- También vemos que, posterior a la etapa de carga, `bbox-1` todavía sigue devolviendo error en algunas respuestas.


Siguiendo el punto anterior, nos gustaría ahora analizar cómo se comportan ante una falla en el sistema. Procederemos ahora a ejecutar escenarios con las siguientes fases:

- Etapa de Warm Up previa, de carga 3 rps durante 10s.
- Etapa de "break" de la aplicación, donde se la someterá a una tasa de 500 rps durante otros 10s, para forzar la caída del servicio.
- Etapa de análisis de recuperación, donde la carga bajará nuevamente a 3 rps durante 30s.

Comenzando por `bbox-1`:

![](imagenes/seccion_2_bbox_1_spike.png)

Vemos que ya en el comienzo de la etapa de quiebre aparecen los primeros requests fallidos. Luego, en la etapa de "meseta" se mantienen algunos requests con status 500 o 504, pero en mucha menor proporción.

Para el caso de `bbox-2`:

![](./imagenes/seccion_2_bbox_2_spike.png)

Ante la misma carga, `bbox-2` responde tan bien que no hemos siquiera logrado romperlo. La capacidad de procesar esa carga pesada es mucho mayor en `bbox-2` que en su contraparte. Para aclarar, nos gustaría remarcar que no hemos podido encontrar un punto donde empezaran a fallar algunos requests para `bbox-2`, sin que la herramienta `artillery` no indicará una advertencia o *warning* que condicione las mediciones.

### Spike leve

Ahora se va analizar como afecta un spike de 200 requests en 10 segundos a ambos endpoints. Se arranca con una carga de 3 requests en 30 segundos, y se pasa a un spike de 200 requests en 10 segundos, para despues volver a una carga de 3 requests en 120 segundos, para ver cuanto tarda en recuperarse. Se puede ver que para el caso de `bbox-1`, puede procesar 50 requests en 10 segundos, mientras que los que sobran entran a pendientes. Por esa razon, de los 200 que entran inicialmente solo puede atender a 50, y de manera constante va atendiendo los que restan. Es por eso que tarda en recuperarse de acuerdo al spike dado, en este caso.

Por otro lado, se puede ver que el tiempo de respuesta es de 1.1 Segundos en condiciones normales, pero aumenta a medida que se le exige la carga.

![](imagenes/seccion_2_bbox_1_spike_a.jpeg)

Para el caso de `bbox-2`, podemos ver que a pesar de tener mas demora para requests normales, de 1.4 Segundos, puede soportar mas carga que el primero, en este caso 170 requests, y dejar solo 27 para pending. Por otro lado el tiempo de respuesta no se vio afectado como el primer caso, donde fue aumentando hasta 40 segundos.

![](imagenes/seccion_2_bbox_1_spike_b.jpeg)


Ambos escenarios fueron corridos con

`yarn phase bbox-1 spike root`

y

`yarn phase bbox-2 spike root`
### **CONCLUSIONES**

Luego de analizar estos resultados, podemos llegar a ciertas conclusiones si comparamos las mediciones de `bbox` con las mediciones obtenidas en la sección 1. Estas son:

- `bbox-1` se comporta más similar a como se comportaba el servicio desarrollado en Gunicorn. Por su parte, `bbox-2` actúa de manera similar al servicio desarrollado en Node.js. Dicho esto, si tuviéramos que clasificar cada una de ellos como Sincrónico o Asincronico, podemos concluir que **`bbox-1` se comporta como un servicio Sincronico**, mientras que **`bbox-2` lo hace como un servicio Asincrónico**.

- Por delay del endpoint, el servicio `bbox-1` tiene un delay de 1.1 s en condiciones normales, pero aumenta en condiciones de carga, mientras que el servicio `bbox-2` tiene un delay de 1.4 s constante inclusive bajo carga.

---

## Sección 3

Para simular el flujo de inscripción se armaron 7 endpoints. Cada uno tiene un tiempo de espera y una cantidad de iteraciones de procesamiento asociado. Con estas variables se intentó simular un servidor real. El tiempo de espera es por las consultas a la base de datos, y las iteraciones de procesamiento simulan validaciones y manipulación de datos.

Los valores elegidos se presentan en la siguiente tabla.

| Endpoint          | Tiempo de espera | Iteraciones de procesamiento |
|-------------------|-----------------:|-----------------------------:|
| GET login         | 0                | 500                          |
| POST login        | 500              | 1000000                      |
| GET carreras      | 1000             | 1000                         |
| GET inscripciones | 1000             | 1000                         |
| GET disponibles   | 1000             | 1000                         |
| POST inscripcion  | 3000             | 10000000                     |
| POST logout       | 0                | 1000000                      |
|

El tiempo de espera son *ms*. Las iteraciones se pueden tomar como unidades relativas para comparar el procesamiento asignado a cada endpoint.

Además, se eligió a Node para implementar la simulación con un load balancer en nginx con 4 réplicas.

Se simularon tres franjas de inscripción de 2 minutos cada una con 3000 alumnos en cada una. Se tiene en cuenta además que al principio de cada franja habrá una carga muy pesada, porque la mayoría de los alumnos se anotarán a penas se les habilita la inscripción. Luego la cantidad de alumnos que ingresan desciende hasta la próxima franja.

Cada alumno se anotará a 5 materias.

Para correr el escenario se debe correr el siguiente comando

```
$ npm run phase node guarani inscripcion
```

#### **RESULTADOS**

Vemos primero el estado de las requests:
![](imagenes/inscripciones/requests_state.png)

Las erroneas:
![](imagenes/inscripciones/requests_errored.png)

La cantidad de errores se relaciona directamente con la cantidad de requests. Al principio de cada franja se observan momentos muy altos. Con una cantidad de errores que llega casi al 75% en los picos.

En los valles en cambio, cuando las rps no son tantas, la cantidad de errores no llega al 50%.

La cantidad de requests completadas si se mantiene mas o menos constante. Con algunos picos y valles que no parecen tener una relacion con las rps.

Las requests pendientes en cambio es creciente. A medida que se suman mas y mas alumnos a la inscripcion el sistema funciona cada vez peor, dejando a varios alumnos esperando.

El tiempo de respuesta en cambio se mantiene constante.

![](imagenes/inscripciones/response_time.png)

Si bien demora bastante (40s en un sistema real seria inaceptable), el tiempo de respuesta se mantuvo constante a pesar de que el servidor estaba bajo mucha carga.

Los errores 500 estuvieron cerca de un 30% de los 200:

![](imagenes/inscripciones/codes_by_request.png)

De vuelta, no se observa mucha relacion entre las rps y los errores 500. Esto tiene sentido viendo el tiempo de respuesta, que es de 40s. Significa que en los momentos de menor carga el sistema sigue procesando las requests de los momentos de mayor carga.
