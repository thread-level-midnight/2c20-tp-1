# Trabajo Práctico 1 - Reentrega

## Sección 1

### Node vs Gunicorn

#### *Intensivo*

En esta sección, hacemos uso del endpoint */intensive* el cual simula el uso intensivo de cálculos y procesamiento de datos por parte del servidor.

A diferencia del primer analisis en la entrega anterior, el escenario ejecutado es tal que se ocupa para ambos servidores el procesador al 100%, pero con un nivel de rps mucho mas bajo. De esta forma se puede evaluar mejor el funcionamiento porque no se esta exigiendo a los servidores algo que nunca podria cumplir. Las requests por segundo se ven en el siguiente grafico:

![](./imagenes/scalability_intensive_scenarios_launched_re.png)

En el proximo grafico se ve ademas que el CPU estaba al 100% para ambos servidores:

![](imagenes/scalability_intensive_gunicorn_re_3.png)

Con este escenario se puede ver claramente una diferencia entre las dos tecnologias.

Para el caso de Node los resultados fueron los siguientes:

![](./imagenes/scalability_intensive_node_re_1.png)
![](./imagenes/scalability_intensive_node_re_2.png)

En Gunicorn:

![](imagenes/scalability_intensive_gunicorn_re_1.png)
![](imagenes/scalability_intensive_gunicorn_re_2.png)

Empezando por el estado de los requests, vemos, tanto en node como en gunicorn que la cantidad de requests que puede completar se mantiene estable, mientras que la cantidad de requests pendientes va subiendo.

Esto tiene sentido porque el procesador esta totalmente ocupado por el procesamiento, y no puede tomar todas las requests.

Aun asi vemos que en gunicorn el promedio de requests que si puede contestar es un poco mayor.

Por otro lado, en node, se ve que llega un punto que la cantidad de requests pendientes se estabiliza, en al rededor de 3.5rps. Al mismo tiempo, empiezan a sugir un monton de errores 504. Estos son errores de timeout lanzados por nginx al no obtener respuesta de node.

Se ve entonces, que node tiene un limite y llegado a un punto no puede ni tomar nuevas requests, en cambio en gunicorn no se llega este limite, y todas las responses tienen un codigo de 200.

(El limite en gunicorn se vio en otras pruebas al aumentar los rps del escenario)

Por ultimo, node mantiene una response time mas o menos estable mientras que gunicorn aumenta con el tiempo y con la cantidad de requests pendientes, sin embargo el promedio de gunicorn es mucho mas bajo.

#### **CONCLUSIONES**

- En operaciones de uso intenso de la CPU, Node parece responder de mejor manera, no solo en cuanto a tiempo y recursos consumidos, sino a la cantidad de respuestas con resultado "exitoso".

La gran conclusion es que gunicorn funciona mucho mejor que node para procesamientos intensivos, no solo logra mantener mejor la estabilidad sin devolver errores, si no que ademas la responsi time, es decir el tiempo que tarda en realizar el procesamiento es menor.




## Seccion 2

En esta seccion se pidio analizar la cantidad de workers para el endpoint sincronico, que habiamos encontrado en el bbox-1. Se hizo un análisis usando el tiempo de demora del endpoint. Sabiendo que tarda en responder mas de 1 segundo (1.2 en promedio), se le sometio a una carga de 3, 4 y 5 requests por segundo, habiendo experimentado previamente que cerca de 4 y 5 requests no podía mantener el tiempo de respuesta establecido. Al ver que en 4 requests por segundo se mantiene estable el tiempo de respuesta, y a partir de 5 requests ya no puede mantenerlo, consideramos que debería tener 5 workers. El razonamiento atras de esta conclusion tiene que ver con que con 4 workers, a 4 request por segundo, solo si el tiempo de respuesta es menor a 1 segundo entonces puede procesar cada request y estar preparado para el proximo. Al ser los requests mayores a 1 segundo, con solamente 4 workers no podría la carga. Este es el razonamiento de 5 requests por segundo, que empieza a no poder responder en el mismo tiempo, y empieza a dejar algun request en pending, por lo tanto aumentando el tiempo de respuesta.

![](imagenes/bbox-delay.png)
